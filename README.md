# **Divercity DevOps Challenge**

Thanks for taking the time to complete this exercise. We are excited that you are considering joining Divercity.

## **Description:**

We've set up some questions for you to answer to the best of your ability, the goal is to check your understanding of some of the things you will work on during your internship.
You will need to edit the markdown file `questions.md` in the quiz folder with your answers.

Not familiar with Markdown? Here are some resources:

- [Getting Started With Markdown](https://www.markdownguide.org/getting-started/)

- [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)

Here's a website to edit markdown and view the output side by side:

- [Dillinger](https://dillinger.io/)


## **Task 1 Description:**

Answer the questions in the Quiz folder by editing the markdown page with your answers.

## **Task 2 Description:**

Sign up for [Qwiklabs](www.qwiklabs.com) with the same email address you will use to submit your assessment
and complete these labs:

- [IAM](https://www.qwiklabs.com/focuses/18123?parent=catalog)

- [LAMBDA](https://www.qwiklabs.com/focuses/16506?parent=catalog)


Take two Screenshots for each lab(You will submit 4 screenshots total) and place them in the submission screenshots folder
1) A screenshot of you in Qwiklabs with the challenge started. Your account name and email will be visible when you click on the "My Account" button on the top right corner of the screen.
An example is in the `example screenshots` folder
![image](https://divercity-assessments.s3.us-west-2.amazonaws.com/QWIKLABS.png)
2) A screenshot of you completing the final step of the project. 
An example for the IAM exercise in the `example screenshots` folder
![image](https://divercity-assessments.s3.us-west-2.amazonaws.com/IAM.png)
3) Optional: A screenshot of the confirmation email qwiklabs sends you for completing the lab
![image](https://divercity-assessments.s3.us-west-2.amazonaws.com/EMAIL.png)


## **Submission :**
- Download this project on bitbucket by clicking the 3 dots next to the clone button.

![image](https://divercity-assessments.s3.us-west-2.amazonaws.com/Bitbucket%20Download.png) 

- Complete the tasks

- Zip your solutions in one folder

- Send the zipped folder containing the modified markdown files with your answers and screenshots to femi@divercity.io


## **NOTE :**
If you have any questions or issues, send an email to femi@divercity.io so he can help clarify.
It's completely fine if you can't finish everything, just explain your blockers in your submission email.
Best Of Luck!




